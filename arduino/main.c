#include "LiquidCrystal.h"

LiquidCrystal lcd_screen(12, 11, 5, 4, 3, 2);


void setup() {
  lcd_screen.begin(16, 2);
  lcd_screen.setCursor(0, 0);
  lcd_screen.print("Access Granted !");
  lcd_screen.setCursor(0, 1);
  lcd_screen.print("Po");
}

void loop() {
  
}

/**
 * status 0 moteur dans la phase d ouverture
 * status 1 moteur dans la phase de fermeture
 * 
 *  @return :
 *  0 l action ne s''st pas effectué correctement
 *  1 l action s'est bien effectué
 */

int motor(int status) {
  
  if (status == 0) {
    return 1;
  }

  if (status == 1) {
    return 1;
  }

  else {
    return 0;
  }
}


package org.isnthelye.httpalexl.isnapp;


import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.v7.app.AlertDialog;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ProgressBar;
import android.app.PendingIntent;

import java.util.HashSet;
import java.util.Properties;
import java.util.Set;


public class LoadingScreen extends Activity {

    private ProgressBar progressBar;

    private int status = 0;


    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);

        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        this.setContentView(R.layout.loading_screen);

        this.progressBar = findViewById(R.id.progressBar);

        final LoadingScreen s = this;

        Thread loadingThread = new Thread() {

            @Override
            public void run() {

                Set<String> devices_saved;

                final Set<BluetoothDevice> devices_found = new HashSet<>();

                try {
                    BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

                    if (bluetoothAdapter == null) {
                        s.openDialog();

                        finishActivity(0);

                    }

                    if (!bluetoothAdapter.isEnabled()) {
                        bluetoothAdapter.enable();
                    }

                    SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences("pref", Context.MODE_PRIVATE);
                    devices_saved = sharedPreferences.getStringSet("devices", null);

                    BroadcastReceiver bluetoothReceiver = new BroadcastReceiver() {
                        @Override
                        public void onReceive(Context context, Intent intent) {
                            String action = intent.getAction();

                            if (BluetoothDevice.ACTION_FOUND.equals(action)) {
                                BluetoothDevice d = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);

                                devices_found.add(d);
                            }
                        }
                    };

                    IntentFilter filter = new IntentFilter(BluetoothDevice.ACTION_FOUND);
                    registerReceiver(bluetoothReceiver, filter);

                    bluetoothAdapter.startDiscovery();

                    Thread.sleep(5000);

                    bluetoothAdapter.cancelDiscovery();
                    unregisterReceiver(bluetoothReceiver);

                    status = 1;
                } catch (Exception e) {
                    e.printStackTrace();
                    finishActivity(0);
                } finally {

                    if (status != 1)
                        finishActivity(0);
                    else {
                        Intent i = new Intent(LoadingScreen.this, MainActivity.class);
                        startActivity(i);
                        finish();
                    }
                }
            }
        };

        loadingThread.start();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    private void openDialog() {
        Looper.prepare();
        Handler handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getApplicationContext());
                builder.setTitle("Wrong device")
                        .setMessage("This app needs bluetooth to perform")
                        .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                finish();
                            }
                        })
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .show();
            }
        };

        Looper.loop();


    }
}
